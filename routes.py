from flask import Flask, render_template, url_for
app = Flask(__name__)

posts = [
    {
        'author': 'Soumitra',
        'title': 'A title',
        'content': 'The Content',
        'date_posted': 'Posted Data'
    },
    {
        'author': 'Soumitra',
        'title': 'A title',
        'content': 'The Content',
        'date_posted': 'Posted Data'
    },
    {
        'author': 'Soumitra',
        'title': 'A title',
        'content': 'The Content',
        'date_posted': 'Posted Data'
    },
]


@app.route("/")
def hello():
    return render_template('home.html', post=posts, title="H")


@app.route("/about")
def about():
    return render_template('about.html')


if __name__ == "__main__":
    app.run()
